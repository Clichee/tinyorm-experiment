mkdir tinybuild
cmake -B tinybuild -DCMAKE_TOOLCHAIN_FILE=$(pwd)/vcpkg/scripts/buildsystems/vcpkg.cmake -DCMAKE_INSTALL_PREFIX=$(pwd)/stage -DCMAKE_BUILD_TYPE=Debug $(pwd)/TinyORM
cmake --build tinybuild
cmake --install tinybuild


mkdir build
cmake -B build -DCMAKE_TOOLCHAIN_FILE=$(pwd)/vcpkg/scripts/buildsystems/vcpkg.cmake -DCMAKE_INSTALL_PREFIX=$(pwd)/stage -DCMAKE_BUILD_TYPE=Debug $(pwd)/src
cmake --build build
cmake --install build


