#include <QDebug>

#ifdef _WIN32
#  include <qt_windows.h>
#endif

#include <orm/db.hpp>
#include <orm/schema.hpp>

#include "CUser.h"

using Orm::DB;
using Orm::Schema;
using namespace Orm::Types;

//Insert Stringlist of values into table "posts"
void insertIntoDb(QStringList names)
{
    QVector<QVariantMap> values;

    Q_FOREACH(QString name, names) {
        values.append(
            QVariantMap{
               {"name", name}
            }
        );
    }

    DB::table("posts")->insert(values);
}

//Create few sample users
QList<CUser::Pointer> userSamples()
{
    QList<CUser::Pointer>  samples;
    samples.append(CUser::Pointer(new CUser(1, "Max", "mustermail@sodgeit.de", 1)));
    samples.append(CUser::Pointer(new CUser(2, "Heinrich", "hein@sodgeit.de", 2)));
    samples.append(CUser::Pointer(new CUser(3, "Eva", "eva@sodgeit.de", 3)));
    samples.append(CUser::Pointer(new CUser(4, "SampleName", "sample@sodgeit.de", 3)));
    samples.append(CUser::Pointer(new CUser(5, "Martin", "martin@sodgeit.de", 4)));
    samples.append(CUser::Pointer(new CUser(6, "Frank", "frank@bar.com", 5)));

    return samples;
}

//Inserts a given list of users into the database
void userdataInsertion(QList<CUser::Pointer> userList)
{
    QVector<QVariantMap> userVector;
    Q_FOREACH(CUser::Pointer cuser, userList) {
        userVector.append(cuser->toVariantMap());
    }
    DB::table("users")->upsert(userVector, {"id"}, {"name", "email", "postsId"});

    return;
}

void selectStatement()
{
    auto posts = DB::select("select * from posts");

    while(posts.next())
        qDebug() << posts.value("id").toULongLong() << posts.value("name").toString();
}

//Extracts all data from the table "user" into a list of User classes
QList<CUser::Pointer> userdataExtraction()
{
    QList<CUser::Pointer> userdata;
    auto users = DB::select("select * from users");

    while(users.next())
    {
        CUser::Pointer user = CUser::Pointer(new CUser(
            QVariantMap{
                {"id", users.value("id")},
                {"name", users.value("name")},
                {"email", users.value("email")},
                {"postsId", users.value("postsId")}
            }
            ));
        userdata.append(user);
    }

    return userdata;
}


int main(int /*unused*/, char */*unused*/[])
{
    try {
    #ifdef _WIN32
        SetConsoleOutputCP(CP_UTF8);
    //    SetConsoleOutputCP(1250);
    #endif

        // Ownership of a shared_ptr()
        auto manager = DB::create({
                                   {"driver",   "QSQLITE"},
                                   {"database", qEnvironmentVariable("DB_DATABASE", "HelloWorld.sqlite3")},
                                   {"check_database_exists", true},
                                   });

        insertIntoDb(QStringList() << "Hans Mustermann" << "Frank Mustermann" << "Eva Mustermann");

        selectStatement();

        Schema::dropIfExists("users");

        //Create table with TinyORM Schema wrapper
        Schema::create("users", [](Orm::SchemaNs::Blueprint &table)
        {
            table.id();
            table.string("name");
            table.string("email").unique();
            table.foreignId("postsId");
        });

        //Insert test values into table 'users'
        DB::table("users")->insert({
            {{"name", "Max Mustermann"}, {"email", "max.mustermann@sodgeit.de"}, {"postsId", 1}},
            {{"name", "Frank Mustermann"}, {"email", "f_mustermann@hotmail.com"}, {"postsId", 3}},
            {{"name", "Eva Mustermann"}, {"email", "eva_Mustermann@gmx.de"}, {"postsId", 4}},
        });

        //
        DB::table("users")->upsert({
            {{"name", "Heinrich Mustermann"}, {"email", "hein.mustermann@sodgeit.de"}, {"postsId", 50}},
            {{"name", "Max Mustermann"}, {"email", "max.mustermann@sodgeit.de"}, {"postsId", 10}},
            {{"name", "Frank Mustermann"}, {"email", "frank.mustermann@hotmail.com"}, {"postsId", 30}},
            {{"name", "Eva Mustermann"}, {"email", "eva.Mustermann@gmx.de"}, {"postsId", 40}},
            },
            {"email"}, {"name", "email", "postsId"});

        //Join tables 'users' and 'posts'
        //join == INNERJOIN
        SqlQuery users = DB::table("users")
                         ->join("posts", "posts.id", "=", "users.postsId")
                         //.join("orders", "users.id", "=", "orders.user_id") //concat joins by chaining
                         .select({"users.*", "posts.name"})
                         .get();

        users.finish();
        //while(users.next())
            // qDebug() << users.value("users.id");//users.value("posts.id").toULongLong() << users.value("users.id").toULongLong() << users.value("name").toString() << users.value("email").toString();

        userdataInsertion(userSamples());

        QList<CUser::Pointer> userdata = userdataExtraction();

        Q_FOREACH(CUser::Pointer user, userdata)
        {
            qDebug() << "UserId: " << user->getId() << " Name: " << user->getName();
        }

        qDebug() << "process finished!";

    } catch (const int &ex) {
        //qCritical() << ex.what();
    }

}
