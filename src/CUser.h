#pragma once

#include <QtCore/QObject>
#include <QtCore/QVariantMap>
#include <QtCore/QSharedPointer>

struct CUser : QObject {
    Q_OBJECT
    Q_PROPERTY(int id READ getId WRITE setId)
    Q_PROPERTY(QString email READ getEmail WRITE setEmail)
    Q_PROPERTY(QString name READ getName WRITE setName)
    Q_PROPERTY(int postsId READ getPostsId WRITE setPostsId)

public:
    using Pointer = QSharedPointer<CUser>;

    enum {
        AUTOGENERATE_ID = -1
    };

    CUser();
    CUser(QVariantMap map);
    CUser(int id, QString name, QString email, int postsId);

    static QVariant getValue(QVariantMap map, QString keyName);
    QVariantMap toVariantMap() const;

    void setEmail(QString);
    QString getEmail() const;

    void setName(QString);
    QString getName() const;

    void setId(int id);
    int getId() const;

    void setPostsId(int id);
    int getPostsId() const;

private:
    int m_id;
    QString m_name;
    QString m_email;
    int m_postsId;
};
