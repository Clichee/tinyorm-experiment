#include <QtCore/QMetaProperty>
#include <QtCore/QRectF>

#include "CUser.h"

CUser::CUser()
    : m_id(-1)
    , m_name()
    , m_email()
    , m_postsId(-1) {
}

CUser::CUser(QVariantMap map)
    : m_id(getValue(map, "id").toInt())
    , m_name(getValue(map, "name").toString())
    , m_email(getValue(map, "email").toString())
    , m_postsId(getValue(map, "postsId").toInt()) {
}

CUser::CUser(int id, QString name, QString email, int postsId)
    : m_id(id)
    , m_name(name)
    , m_email(email)
    , m_postsId(postsId) {
}

QVariant CUser::getValue(QVariantMap map, QString keyName) {
    if (map.contains(keyName)) {
        return map[keyName];
    }
    throw std::out_of_range(keyName.toStdString());
}

QVariantMap CUser::toVariantMap() const {
    QVariantMap m;
    const QMetaObject *metaobject = this->metaObject();
    int count = metaobject->propertyCount();
    for (int i=0; i<count; ++i) {
        QMetaProperty metaproperty = metaobject->property(i);
        const char *name = metaproperty.name();
        QVariant value = this->property(name);
        if(QString(name) == "objectName") {
            continue;
        }
        if(QString(name) == "id") {
            if(value.toInt() == AUTOGENERATE_ID) {
                continue;
            }
        }
        m.insert(name, value);
    }
    return m;
}

void CUser::setId(int id) { m_id = id; }
int CUser::getId() const { return m_id; }
void CUser::setEmail(QString email) { m_email = email; }
QString CUser::getEmail() const { return m_email; }
void CUser::setName(QString name) { m_name = name; }
QString CUser::getName() const { return m_name; }
void CUser::setPostsId(int postsId) { m_postsId = postsId; }
int CUser::getPostsId() const { return m_postsId; }
