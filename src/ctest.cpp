#include "ctest.h"

using Orm::DB;
using Orm::Schema;
using namespace Orm::Types;

CTest::CTest()
    : m_connection()
{
    qDebug() << "Hello World";
}

void CTest::initTestCase()
{
    m_connection = DB::create({
        {"driver",   "QSQLITE"},
        {"database", qEnvironmentVariable("DB_DATABASE", "HelloWorld.sqlite3")},
        {"check_database_exists", true},
    });

    DB::disableQueryLog(m_connection->getDefaultConnection());
}

void createSamples(Orm::SchemaNs::Blueprint &table) {
    table.id();
    table.string("s");
    table.string("n").nullable();
    table.string("u").nullable().unique();
}

void createSamples2(Orm::SchemaNs::Blueprint &table) {
    table.id();
    table.integer("required");
    table.string("nullable").nullable();
    table.string("unique").nullable().unique();
}

void createSamples3(Orm::SchemaNs::Blueprint &table) {
    table.id();
    table.integer("value");
    table.foreignId("prepopulatedId");
}

void prepopulate(std::shared_ptr<Orm::Query::Builder> table)
{
    table->insert(QVariantMap{ { "id", 1 }, {"required", 0}, { "nullable", "foobar" }, { "unique", "Hello" } });
    table->insert(QVariantMap{ { "id", 2 }, {"required", 2}, { "nullable", "foobar" }, { "unique", "World" } });
    table->insert(QVariantMap{ { "id", 3 }, {"required", 1}, { "nullable", QVariant() },  { "unique", QVariant() }  });
}

void prepopulate2(std::shared_ptr<Orm::Query::Builder> table)
{
    table->insert(QVariantMap{ { "id", 1 }, {"value", 30}, { "prepopulatedId", 3 } });
    table->insert(QVariantMap{ { "id", 2 }, {"value", 20}, { "prepopulatedId", 2 } });
    table->insert(QVariantMap{ { "id", 3 }, {"value", 10}, { "prepopulatedId", 1 } });
}

void CTest::init()
{
    Schema::dropIfExists("users");

    //Create table with TinyORM Schema wrapper
    Schema::create("users", [](Orm::SchemaNs::Blueprint &table)
                   {
                       table.id();
                       table.string("name");
                       table.string("email").unique();
                       table.foreignId("postsId");
                   });

    Schema::dropIfExists("samples");
    Schema::create("samples", createSamples);
    qDebug("table dropped");

    Schema::dropIfExists("prepopulated");
    Schema::create("prepopulated", createSamples2);
    prepopulate(DB::table("prepopulated"));

    Schema::dropIfExists("prepopulated2");
    Schema::create("prepopulated2", createSamples3);
    prepopulate2(DB::table("prepopulated2"));
}

void CTest::create()
{
    QFETCH(QVector<QString>, tableNames);
    QFETCH(bool, expectException);
    QFETCH(QVector<QVariant>, expectedS);


    bool exceptionOccured(false);
    try {
        Q_FOREACH(QString tableName, tableNames)
        {
            Schema::dropIfExists(tableName);
            Schema::create(tableName, [](Orm::SchemaNs::Blueprint &table)
            {
               table.id();
               table.string("val");
               table.bigInteger("null").nullable();
               table.boolean("uniq").unique();
               table.foreignId("postsId");
            });
        }
        exceptionOccured=false;
    } catch (const std::exception &ex) {
        qDebug() << ex.what();
        exceptionOccured=true;
    }

    QVector<QVariant> pluckedS = DB::table(tableNames[0])->pluck("val");

    QCOMPARE(exceptionOccured, expectException);
    QCOMPARE(pluckedS, expectedS);
}

void CTest::create_data()
{
    QTest::addColumn< QVector<QString> >("tableNames");
    QTest::addColumn<bool>("expectException");
    QTest::addColumn<QVector<QVariant> >("expectedS");

    QTest::addRow("Simple create")
        << QVector<QString>("tempTable")
        << false << QVector<QVariant>{};

    QTest::addRow("Multiple creates")
        << (QVector<QString>() << "tempTable" << "tempTable2")
        << false << QVector<QVariant>{};
}

void CTest::drop()
{
    QFETCH(QVector<QString>, tableNames);
    QFETCH(bool, expectException);


    bool exceptionOccured(false);
    try {
        Q_FOREACH(QString tableName, tableNames)
        {
            Schema::dropIfExists(tableName);
        }
        exceptionOccured=false;
    } catch (const std::exception &ex) {
        qDebug() << ex.what();
        exceptionOccured=true;
    }

    QCOMPARE(exceptionOccured, expectException);
}

void CTest::drop_data()
{
    QTest::addColumn< QVector<QString> >("tableNames");
    QTest::addColumn<bool>("expectException");

    QTest::addRow("Single drop")
        << QVector<QString>("samples")
        << false;

    QTest::addRow("Multiple drops")
        << (QVector<QString>() << "samples" << "prepopulated" << "prepopulated2")
        << false;

    QTest::addRow("Non existant table")
        << QVector<QString>("samples2")
        << false;
}

void CTest::insert()
{
    QFETCH(QString, tableName);
    QFETCH(QVector<QVariantMap>, input);
    QFETCH(QVector<QVariant>, expectedS);
    QFETCH(bool, expectException);

    bool exceptionOccured(false);
    try {
        DB::table(tableName)->insert(input);
        exceptionOccured=false;
    } catch (const std::exception &ex) {
        qDebug() << ex.what();
        exceptionOccured=true;
    }

    QString col = (tableName == "samples") ? ("s") : ("required");
    QVector<QVariant> pluckedS = DB::table(tableName)->pluck(col);

    QCOMPARE(exceptionOccured, expectException);
    QCOMPARE(pluckedS, expectedS);
}

void CTest::insert_data()
{
    QTest::addColumn<QString>("tableName");
    QTest::addColumn<QVector<QVariantMap> >("input");
    QTest::addColumn<bool>("expectException");
    QTest::addColumn<QVector<QVariant> >("expectedS");

    QTest::newRow("good samples")
        << "samples"
        << QVector<QVariantMap>{ QVariantMap{ {"s", "A"} } }
        << false
        << QVector<QVariant>{ "A" };
    QTest::newRow("bad sample")
        << "samples"
        << QVector<QVariantMap>{ QVariantMap{ {"X", "A"} } }
        << true
        << QVector<QVariant>{};

    QTest::newRow("identical unique values")
        << "samples"
        << ( QVector<QVariantMap>()
            << QVariantMap{ {"s", "A"}, {"u", "A"} }
            << QVariantMap{ {"s", "B"}, {"u", "A"} }
            )
        << true
        << QVector<QVariant>{};

    QTest::newRow("identical allowed null values")
        << "samples"
        << ( QVector<QVariantMap>()
            << QVariantMap{ {"s", "A"}}
            << QVariantMap{ {"s", "B"}}
            )
        << false
        << QVector<QVariant>{"A", "B"};

    QTest::newRow("invalid null values")
        << "samples"
        << ( QVector<QVariantMap>()
            << QVariantMap{ {"n", "B"}, {"u", "C"} }
            << QVariantMap{ {"s", "B"}, {"n", "C"}, {"u", "B"} }
            )
        << true
        << QVector<QVariant>{};

    QTest::newRow("Input already exists")
        << "prepopulated"
        << ( QVector<QVariantMap>()
            << QVariantMap{ {"id", 1}, {"required", "A"} }
            )
        << true
        << QVector<QVariant>{ 0, 2, 1 };
}

void CTest::update()
{
    QFETCH(QVector<QVariantMap>, whereEq);
    QFETCH(QVector<Orm::UpdateItem>, update);
    QFETCH(QVector<QVariant>, expectedS);
    QFETCH(bool, expectException);
    QString tableName("prepopulated");

    QString col = (tableName == "samples") ? ("s") : ("nullable");

    bool exceptionOccured(false);
    try {
        Orm::QueryBuilder q = DB::table(tableName)->whereEq(whereEq[0].firstKey(), whereEq[0].first());
        whereEq.remove(0);
        if(!whereEq.isEmpty())
        {
            Q_FOREACH(QVariantMap whereStmt, whereEq)
            {
                q.whereEq(whereStmt.firstKey(), whereStmt.first());
            }
        }

        q.update(update);
        exceptionOccured=false;
    } catch (const std::exception &ex) {
        qDebug() << ex.what();
        exceptionOccured=true;
    }

    QVector<QVariant> pluckedS = DB::table(tableName)->pluck(col);
    qDebug() << pluckedS;

    QCOMPARE(exceptionOccured, expectException);
    QCOMPARE(pluckedS, expectedS);
}

void CTest::update_data()
{
    QTest::addColumn<QVector<QVariantMap> >("whereEq");
    QTest::addColumn<QVector<Orm::UpdateItem> >("update");
    QTest::addColumn<bool>("expectException");
    QTest::addColumn<QVector<QVariant> >("expectedS");

    QTest::newRow("valid update, 1 where stmt")
        << ( QVector<QVariantMap>()
            << QVariantMap{ {"required", 1} } )
        << QVector<Orm::UpdateItem>{ { {"nullable", "updated"} } }
        << false << QVector<QVariant>{ "foobar", "foobar", "updated" };

    QTest::newRow("valid update, 2 where stmt")
        << ( QVector<QVariantMap>()
            << QVariantMap{ {"id", 1} }
            << QVariantMap{ {"nullable", "foobar"} } )
        << QVector<Orm::UpdateItem>{ { {"nullable", "updated"} } }
        << false << QVector<QVariant>{ "updated", "foobar", "" };

    QTest::newRow("valid update on empty query")
        << ( QVector<QVariantMap>()
            << QVariantMap{ {"required", 3} } )
        << QVector<Orm::UpdateItem>{ { {"nullable", "updated"} } }
        << false << QVector<QVariant>{ "foobar", "foobar", "" };

    QTest::newRow("invalid update, wrong type")
        << ( QVector<QVariantMap>()
            << QVariantMap{ {"id", 1} } )
        << QVector<Orm::UpdateItem>{ { {"required", QVariant()} } }
        << true << QVector<QVariant>{"foobar", "foobar", ""};

    QTest::newRow("invalid update, not unqiue")
        << ( QVector<QVariantMap>()
            << QVariantMap{ {"id", 1} } )
        << QVector<Orm::UpdateItem>{ { {"unique", "World"} } }
        << true << QVector<QVariant>{"foobar", "foobar", ""};
}

void CTest::upsert()
{
    QFETCH(QString, tableName);
    QFETCH(QVector<QVariantMap>, upsert);
    QFETCH(QVector<QString>, uniq);
    QFETCH(QVector<QString>, update);
    QFETCH(QVector<QVariant>, expectedS);
    QFETCH(bool, expectException);

    bool exceptionOccured(false);
    try {
        DB::table(tableName)->upsert(upsert, uniq, update);
        exceptionOccured=false;
    } catch (const std::exception &ex) {
        qDebug() << ex.what();
        exceptionOccured=true;
    }

    QString col = (tableName == "samples") ? ("s") : ("required");
    QVector<QVariant> pluckedS = DB::table(tableName)->pluck(col);
    qDebug() << pluckedS;

    QCOMPARE(exceptionOccured, expectException);
    QCOMPARE(pluckedS, expectedS);
}

void CTest::upsert_data()
{
    QTest::addColumn<QString>("tableName");
    QTest::addColumn<QVector<QVariantMap> >("upsert");
    QTest::addColumn<QVector<QString> >("uniq");
    QTest::addColumn<QVector<QString> >("update");
    QTest::addColumn<bool>("expectException");
    QTest::addColumn<QVector<QVariant> >("expectedS");

    QTest::newRow("simple insert only")
        << "samples"
        << ( QVector<QVariantMap>()
            << QVariantMap{ {"s", "Hello"}, {"n", "World"}, {"u", "!"} } )
        << QVector<QString>{"id"}
        << QVector<QString>{"s", "n", "u"}
        << false << QVector<QVariant>{"Hello"};

    QTest::newRow("simple update only")
        << "prepopulated"
        << ( QVector<QVariantMap>()
            << QVariantMap{{"id", 2}, {"required", 20}, {"nullable", "Hello"}, {"unique", "World?"} } )
        << QVector<QString>{"id"}
        << QVector<QString>{"required", "nullable", "unique"}
        << false << QVector<QVariant>{0, 20, 1};

    QTest::newRow("insert + update")
        << "prepopulated"
        << ( QVector<QVariantMap>()
            << QVariantMap{ {"id", 4}, {"required", 40}, {"nullable", "Who"}, {"unique", "Is"} }
            << QVariantMap{ {"id", 5}, {"required", 50}, {"nullable", "There"}, {"unique", "?"} }
            << QVariantMap{ {"id", 4}, {"required", 19238961}, {"nullable", "I"}, {"unique", "Am!"} } )
        << QVector<QString>{"id"}
        << QVector<QString>{"required", "nullable", "unique"}
        << false << QVector<QVariant>{0, 2, 1, 19238961, 50};

    QTest::newRow("update to invalid value")
        << "prepopulated"
        << ( QVector<QVariantMap>()
            << QVariantMap{ {"id", 4}, {"required", QVariant()}, {"nullable", "Who"}, {"unique", "Hello"} } )
        << QVector<QString>{"id"}
        << QVector<QString>{"required", "nullable", "unique"}
        << true << QVector<QVariant>{0, 2, 1};

    QTest::newRow("upsert non existant column")
        << "prepopulated"
        << ( QVector<QVariantMap>()
            << QVariantMap{ {"id", 12}, {"require", "something"}, {"nullable", "Who"}, {"unique", "brrr"} } )
        << QVector<QString>{"id"}
        << QVector<QString>{"required", "nullable", "unique"}
        << true << QVector<QVariant>{0, 2, 1};
}

void CTest::select()
{
    QFETCH(QVector<QString>, join);
    QFETCH(QVector<Orm::Column>, columns);
    QFETCH(QVector<QString>, where);
    QFETCH(QVector<QString>, order);
    QFETCH(QString, group);
    QFETCH(QVector<QVariant>, expectedS);
    QFETCH(bool, expectException);
    QString tableName("prepopulated");

    QString col = (tableName == "samples") ? ("s") : ("nullable");

    QVector<QVariant> res;
    bool exceptionOccured(false);
    try {
        Orm::QueryBuilder q = DB::table(tableName)->select(columns);

        if(!join.isEmpty()){
            QString arg2 = tableName + ".id";
            QString arg4 = join[0] + "." + tableName + "Id";
            q.join(join[0], arg2, "=", arg4);
        }

        if(!where.isEmpty())
        {
            q.where(where[0], where[1], where[2]);
        }

        if(!order.isEmpty()){
            q.orderBy(order[0], order[1]);
        }

        if(!group.isEmpty()){
            q.groupBy(group);
        }

        res = q.pluck(col);
        exceptionOccured=false;
    } catch (const std::exception &ex) {
        qDebug() << ex.what();
        exceptionOccured=true;
    }

    qDebug() << res;

    QCOMPARE(exceptionOccured, expectException);
    QCOMPARE(res, expectedS);
}

void CTest::select_data()
{
    QTest::addColumn<QVector<QString> >("join");
    QTest::addColumn<QVector<Orm::Column> >("columns");
    QTest::addColumn<QVector<QString> >("where");
    QTest::addColumn<QVector<QString> >("order");
    QTest::addColumn<QString >("group");
    QTest::addColumn<bool>("expectException");
    QTest::addColumn<QVector<QVariant> >("expectedS");

    QTest::newRow("valid simple select")
        << QVector<QString>()
        << QVector<Orm::Column>({"prepopulated.id", "prepopulated.nullable"})
        << QVector<QString>()
        << QVector<QString>()
        << QString()
        << false << QVector<QVariant>{ "foobar", "foobar", "" };

    QTest::newRow("valid select with join")
        << QVector<QString>("prepopulated2")
        << QVector<Orm::Column>({"prepopulated.nullable"})
        << QVector<QString>()
        << QVector<QString>()
        << QString()
        << false << QVector<QVariant>{ "", "foobar", "foobar" };

    QTest::newRow("valid select with where")
        << QVector<QString>()
        << QVector<Orm::Column>({"prepopulated.nullable"})
        << (QVector<QString>()
            << "nullable"
            << "="
            << "foobar")
        << QVector<QString>()
        << QString()
        << false << QVector<QVariant>{"foobar", "foobar"};

    QTest::newRow("select non existant column")
        << QVector<QString>()
        << QVector<Orm::Column>({"notInDb"})
        << QVector<QString>()
        << QVector<QString>()
        << QString()
        << false << QVector<QVariant>{QVariant(), QVariant(), QVariant()};

    QTest::newRow("empty selection")
        << QVector<QString>()
        << QVector<Orm::Column>({"nullable"})
        << (QVector<QString>()
            << "nullable"
            << "="
            << "foo")
        << QVector<QString>()
        << QString()
        << false << QVector<QVariant>{};

    QTest::newRow("non existant where column")
        << QVector<QString>()
        << QVector<Orm::Column>({"nullable"})
        << (QVector<QString>()
            << "nonExistant"
            << "="
            << "foo")
        << QVector<QString>()
        << QString()
        << false << QVector<QVariant>{};

    QTest::newRow("non existant join table")
        << QVector<QString>("nonExistant")
        << QVector<Orm::Column>({"nullable"})
        << QVector<QString>()
        << QVector<QString>()
        << QString()
        << true << QVector<QVariant>{};

    QTest::newRow("Order by id desc")
        << QVector<QString>()
        << QVector<Orm::Column>({"id", "nullable"})
        << QVector<QString>()
        << (QVector<QString>() << "id" << "desc")
        << QString()
        << false << QVector<QVariant>{"", "foobar", "foobar"};

    QTest::newRow("group by nullable")
        << QVector<QString>()
        << QVector<Orm::Column>({"nullable"})
        << QVector<QString>()
        << QVector<QString>()
        << QString("nullable")
        << false << QVector<QVariant>{"", "foobar"};
}

void CTest::remove()
{
    QFETCH(QVector<QString>, where);
    QFETCH(int, id);
    QFETCH(QVector<QVariant>, expectedS);
    QFETCH(bool, expectException);
    QString tableName("prepopulated");

    QString col = (tableName == "samples") ? ("s") : ("nullable");

    bool exceptionOccured(false);
    try {
        Orm::QueryBuilder q = DB::table(tableName)->where(where[0], where[1], where[2]);

        if(id == -1)
            q.remove();
        else
            q.remove(id);

        exceptionOccured=false;
    } catch (const std::exception &ex) {
        qDebug() << ex.what();
        exceptionOccured=true;
    }

    QVector<QVariant> res = DB::table(tableName)->pluck(col);

    qDebug() << res;

    QCOMPARE(exceptionOccured, expectException);
    QCOMPARE(res, expectedS);
}

void CTest::remove_data()
{
    QTest::addColumn<QVector<QString> >("where");
    QTest::addColumn<int>("id");
    QTest::addColumn<bool>("expectException");
    QTest::addColumn<QVector<QVariant> >("expectedS");

    QTest::newRow("delete all entries")
        << (QVector<QString>()
            << "id"
            << ">"
            << "0")
        << -1
        << false << QVector<QVariant>{};

    QTest::newRow("delete 1 entries")
        << (QVector<QString>()
            << "id"
            << "="
            << "1")
        << -1
        << false << QVector<QVariant>{"foobar", ""};

    QTest::newRow("delete 1 entries by id")
        << (QVector<QString>()
            << "id"
            << ">"
            << "0")
        << 1
        << false << QVector<QVariant>{"foobar", ""};

    QTest::newRow("delete by id with missing entry")
        << (QVector<QString>()
            << "id"
            << ">"
            << "1")
        << 1
        << false << QVector<QVariant>{"foobar", "foobar", ""};

    QTest::newRow("delete by negative id")
        << (QVector<QString>()
            << "id"
            << ">"
            << "1")
        << -2
        << false << QVector<QVariant>{"foobar", "foobar", ""};
}


QTEST_MAIN(CTest)
