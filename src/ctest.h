#ifndef CTEST_H
#define CTEST_H

#include <QObject>
#include <QTest>

#include <orm/db.hpp>
#include <orm/schema.hpp>

class CTest : public QObject
{
    Q_OBJECT
public:
    CTest();

protected:
    std::shared_ptr<Orm::DatabaseManager> m_connection;

private Q_SLOTS:
    void initTestCase();
    void init();

    void create();
    void create_data();

    void drop();
    void drop_data();

    void insert();
    void insert_data();

    void update();
    void update_data();

    void upsert();
    void upsert_data();

    void select();
    void select_data();

    void remove();
    void remove_data();
};

#endif // CTEST_H
